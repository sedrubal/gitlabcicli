# -*- coding: utf-8 -*-

"""Command line interface for GitLab CI to query the current buildstatus."""

__project__ = "gitlabcicli"
__author__ = "sedrubal"
__email__ = "dev@sedrubal.de"
__url__ = f"https://gitlab.com/{__author__}/{__project__}"
__license__ = "CC BY SA 4.0"
__version__ = "0.1"
