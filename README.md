# GitLab CI Cli

Command line interface for GitLab CI

## Usage

```bash
$> pip install --user --upgrade gitlabcicli
$> gitlabcicli --token 1234DEADBEEF [--server https://git.example.com/] [--commit 12345678] [--project gitlab-org/gitlab-ce]
  id  status    stage    commit hash    commit title  author    coverage
----  --------  -------  -------------  ------------  --------  ----------
 211  success   test     ae857aa9       Bug Fix       John Doe  75.6%
 212  success   test     ae857aa9       Bug Fix       John Doe  75.6%
$>
```

```bash
usage: gitlabcicli [-h] [--version] [-v] [--token TOKEN] [--server URL]
                   [--project PROJECT]
                   {lint,show,do,raw} ...

Command line interface for GitLab CI

optional arguments:
  -h, --help          show this help message and exit
  --version           show program's version number and exit
  -v                  Be more verbose (up to -vvvvv)
  --token TOKEN       The token from gitlab to query the API
  --server URL        The gitlab server url(e.g.: https://gitlab.com/,
                      default: parsed from remote")
  --project PROJECT   The project name (e.g. foo/bar, default: current
                      project)

actions:
  {lint,show,do,raw}
    show              Show the current job status
    raw               Get or watch the output of a job
    do                Run actions on jobs [cancel, retry, erase]
    lint              Validate the gitlab-ci.yml
```

## LICENSE

[GPLv3](./LICENSE)
